#!/bin/sh

export LD_LIBRARY_PATH="${PWD}/subprojects/TIM-VX/prebuilt-sdk/x86_64_linux/lib"
export VIVANTE_SDK_DIR="${PWD}/subprojects/TIM-VX/prebuilt-sdk/x86_64_linux"

# This should be the NPU in i.MX8MP
export VSIMULATOR_CONFIG=VIP8000NANOSI_PLUS_PID0X9F

CONV="$(basename "$0" .sh)"

# Run
build/src/"${CONV}" "$@"

# Run again, saving network_binary.nb. This results in all-zero output.
VIV_VX_ENABLE_SAVE_NETWORK_BINARY=1 build/src/"${CONV}" "$@" > /dev/null
mv network_binary.nb "${CONV}.nb"
echo "nb file stored as ${CONV}.nb"
