#!/bin/env python3
#
# Open a precompiled Network Binary (.nb) file and dump info about it.
#

import sys
import struct

def get_u32(b):
    return b[0] | (b[1] << 8) | (b[2] << 16) | (b[3] << 24)

class BinaryEntry:
    def __init__(self, offset, size):
        self.offset = offset
        self.size = size

def get_table(b):
    offset = get_u32(b[0:4])
    size = get_u32(b[4:8])
    return BinaryEntry(offset, size)

class BitStream:
    def __init__(self, num_bits, byte_stream):
        self.num_bits = num_bits
        self.byte_stream = byte_stream
        self.bit_index = 0

    def get_bit(self):
        bit_index = self.bit_index
        self.bit_index += 1
        return (self.byte_stream[bit_index // 8] >> (bit_index % 8)) & 1

    def get_bits(self, num_bits):
        value = 0
        for i in range(num_bits):
            value |= self.get_bit() << i
        return value

    def __str__(self):
        return ''.join(['01'[(self.byte_stream[bit // 8] >> (bit % 8)) & 1] for bit in range(self.num_bits)])

class CompressedKernel:
    def __init__(self, data):
        self.bit16 = (data[0] >> 1) & 1
        self.fp16 = (data[0] >> 2) & 1
        self.version = (data[0] >> 4) & 0xf
        if self.version != 1:
            print('Unsupported kernel version')
            sys.exit(1)
        self.zero_run_length_size = data[1]
        if self.zero_run_length_size > 18:
            print('Invalid zero run length size')
            sys.exit(1)
        self.rlt = [x for x in data[2:2+self.zero_run_length_size]]
        self.map = [data[20 + i // 2] >> (4 * (i & 1)) & 0xf for i in range(8)]
        self.avg_bias = struct.unpack('<Hxx', data[24:28])[0]
        self.stream_size = list(struct.unpack('<IIIIIIII', data[28:60]))
        stream_bytes = (self.stream_size[0] + 7) // 8
        self.stream = data[64:64 + stream_bytes]
        self.footer = struct.unpack('<I', data[128:132])[0]

    def __str__(self):
        bs = BitStream(self.stream_size[0], self.stream)
        lines = [
            f' version = {self.version}',
            f' rlt = {self.rlt}',
            f' map = {self.map}',
            f' avg_bias = {self.avg_bias}',
            f' stream_size = {self.stream_size}',
            ' stream = ' + ', '.join([f'0x{x:02x}' for x in self.stream]),
            ' bits = ' + str(bs),
            f' footer = 0x{self.footer:08x}',
        ]
        return '\n'.join(lines)

class NBG:
    def __init__(self, filename):
        f = open(filename, 'rb')

        # Header
        magic = f.read(4)
        if magic != b'VPMN':
            print('Invalid magic, file is not a Verisilicon Network Binary Graph')
            sys.exit(1)
        header = struct.unpack('<II64sIIII', f.read(4+4+64+4+4+4+4))
        self.version, self.hardware_target, network_name, self.layer_count, \
        self.operation_count, self.input_count, self.output_count = header
        self.network_name = network_name.rstrip(b'\0').decode('ascii')

        if self.version > 0x0001001e:
            print('Unsupported NBG version')
            sys.exit(1)

        # Feature DB
        if self.version >= 0x00010003:
            feature_db = struct.unpack('<IIB3xBB2xI', f.read(4+4+1+3+1+1+2+4))
            feature_bits, self.num_pixel_pipes, self.core_count, self.maybe_nn_core_count, \
            self.maybe_tp_core_count, self.checksum = feature_db
            self.hi_reorder_fix = feature_bits & 1
            self.ocb_counter = (feature_bits >> 1) & 1
            self.nn_command_size = [128, 192, -1, -1][(feature_bits >> 2) & 3]
            self.change_ppu_param = (feature_bits >> 4) & 1
            f.read(44 + 192) # verisilicon and customer reserved

        # Memory pool
        self.pool_size, self.pool_alignment, self.pool_base = struct.unpack('<III', f.read(4+4+4))

        # SRAM
        self.axi_sram_base, self.axi_sram_size = struct.unpack('<II', f.read(4+4))
        if self.version >= 0x00010008:
            self.vip_sram_base, self.vip_sram_size = struct.unpack('<II', f.read(4+4))

        # Binary entries
        self.input_table = get_table(f.read(4+4))
        self.output_table = get_table(f.read(4+4))
        self.layer_table = get_table(f.read(4+4))
        self.operation_table = get_table(f.read(4+4))
        self.lcd_table = get_table(f.read(4+4)) # Load Config Data
        self.lcd = get_table(f.read(4+4))
        self.nn_op_data = get_table(f.read(4+4))
        self.tp_op_data = get_table(f.read(4+4))
        self.sh_op_data = get_table(f.read(4+4))
        self.patch_data = get_table(f.read(4+4))
        self.layer_param_table = get_table(f.read(4+4))
        self.sw_op_data_table = get_table(f.read(4+4))
        if self.version >= 0x0001000c:
            self.hw_init_op_table = get_table(f.read(4+4))
            self.icd_table = get_table(f.read(4+4)) # Initialize Config Data
            self.icd = get_table(f.read(4+4))
        if self.version >= 0x0001000e:
            self.ppu_param_table = get_table(f.read(4+4))
        # knowledge cutoff as of TIM-VX v1.2.6
        if self.version >= 0x0001001e:
            # Unknown entries
            hexdump(f.read(6*(4+4)))
            self.maybe_compressed_kernel_table = get_table(f.read(4+4))
            self.maybe_compressed_kernel = get_table(f.read(4+4))
            # Unknown entries
            f.read(3*(4+4))

        # Load binary entries
        for bin_entry in self.input_table, self.output_table, self.layer_table, \
                self.operation_table, self.nn_op_data, self.icd_table, \
                self.icd, self.maybe_compressed_kernel_table, \
                self.maybe_compressed_kernel:
            self.read_table(f, bin_entry)

        self.parsed_kernel = CompressedKernel(self.maybe_compressed_kernel.data)

    def read_table(self, f, entry):
        f.seek(entry.offset)
        entry.data = f.read(entry.size)

    def __str__(self):
        lines = [
            f'version = {self.version >> 16}.{self.version & 0xffff}',
            f'hardware_target = 0x{self.hardware_target:02x}',
            f'network_name = \'{self.network_name}\'',
            f'layer_count = {self.layer_count}',
            f'operation_count = {self.operation_count}',
            f'input_count = {self.input_count}',
            f'output_count = {self.output_count}',
            f'hi_reorder_fix = {self.hi_reorder_fix}',
            f'ocb_counter = {self.ocb_counter}',
            f'nn_command_size = {self.nn_command_size} bytes',
            f'change_ppu_param = {self.change_ppu_param}',
            f'num_pixel_pipes = {self.num_pixel_pipes}',
            f'core_count = {self.core_count}',
            f'maybe_nn_core_count = {self.maybe_nn_core_count}',
            f'maybe_tp_core_count = {self.maybe_tp_core_count}',
            # f'checksum = 0x{self.checksum:08x}'
        ]
        if self.pool_size:
            lines += [
                f'pool_size = {self.pool_size}',
                f'pool_alignment = {self.pool_alignment}',
                f'pool_base = {self.pool_base}'
            ]
        if self.axi_sram_size:
            lines += [
                f'axi_sram_base = {self.axi_sram_base}',
                f'axi_sram_size = {self.axi_sram_size}'
            ]
        if self.vip_sram_size:
            lines += [
                f'vip_sram_base = {self.vip_sram_base}',
                f'vip_sram_size = {self.vip_sram_size}'
            ]
        return '\n'.join(lines)

def hexdump(b):
    for i in range(0, len(b), 4):
        value = get_u32(b[i:i+4])
        print(f' 0x{value:08x},', end=['','\n'][i % 16 == 12])
    if len(b) % 16:
        print('')

filename = sys.argv[1] if len(sys.argv) > 1 else 'network_binary.nb'
nbg = NBG(filename)
print(nbg)
if nbg.input_table.size:
    print(f'input_table = {nbg.input_table.offset} {nbg.input_table.size}')
if nbg.output_table.size:
    print(f'output_table = {nbg.output_table.offset} {nbg.output_table.size}')
if nbg.layer_table.size:
    print(f'layer_table = {nbg.layer_table.offset} {nbg.layer_table.size}')
if nbg.operation_table.size:
    print(f'operation_table = {nbg.operation_table.offset} {nbg.operation_table.size}')
if nbg.lcd_table.size:
    print(f'lcd_table = {nbg.lcd_table.offset} {nbg.lcd_table.size}')
if nbg.lcd.size:
    print(f'lcd = {nbg.lcd.offset} {nbg.lcd.size}')
if nbg.nn_op_data.size:
    print(f'nn_op_data = {nbg.nn_op_data.offset} {nbg.nn_op_data.size}')
    hexdump(nbg.nn_op_data.data)
if nbg.tp_op_data.size:
    print(f'tp_op_data = {nbg.tp_op_data.offset} {nbg.tp_op_data.size}')
if nbg.sh_op_data.size:
    print(f'sh_op_data = {nbg.sh_op_data.offset} {nbg.sh_op_data.size}')
if nbg.patch_data.size:
    print(f'patch_data = {nbg.patch_data.offset} {nbg.patch_data.size}')
if nbg.layer_param_table.size:
    print(f'layer_param_table = {nbg.layer_param_table.offset} {nbg.layer_param_table.size}')
if nbg.sw_op_data_table.size:
    print(f'sw_op_data_table = {nbg.sw_op_data_table.offset} {nbg.sw_op_data_table.size}')
if nbg.hw_init_op_table.size:
    print(f'hw_init_op_table = {nbg.hw_init_op_table.offset} {nbg.hw_init_op_table.size}')
if nbg.icd_table.size:
    print(f'icd_table = {nbg.icd_table.offset} {nbg.icd_table.size}')
if nbg.icd.size:
    print(f'icd = {nbg.icd.offset} {nbg.icd.size}')
    # TODO: analyze icd, it contains complete operations
if nbg.ppu_param_table.size:
    print(f'ppu_param_table = {nbg.ppu_param_table.offset} {nbg.ppu_param_table.size}')
if nbg.maybe_compressed_kernel_table.size:
    print(f'compressed_kernel_table = {nbg.maybe_compressed_kernel_table.offset} {nbg.maybe_compressed_kernel_table.size}')
    hexdump(nbg.maybe_compressed_kernel_table.data)
if nbg.maybe_compressed_kernel.size:
    print(f'compressed_kernel = {nbg.maybe_compressed_kernel.offset} {nbg.maybe_compressed_kernel.size}')
    print(nbg.parsed_kernel)
    hexdump(nbg.maybe_compressed_kernel.data)
