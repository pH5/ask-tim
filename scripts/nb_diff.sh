#!/bin/sh
if [ ! -f "$1" -o ! -f "$2" ]; then
	echo "usage: nb_diff FILE1 FILE2"
	exit 1
fi
scripts/nb_dump.py "$1" > "$1.tmp"
scripts/nb_dump.py "$2" > "$2.tmp"
git diff --no-index --color -u "$1.tmp" "$2.tmp"
rm "$1.tmp" "$2.tmp"
