/****************************************************************************
*
*    Copyright (c) 2020-2023 Vivante Corporation
*
*    Permission is hereby granted, free of charge, to any person obtaining a
*    copy of this software and associated documentation files (the "Software"),
*    to deal in the Software without restriction, including without limitation
*    the rights to use, copy, modify, merge, publish, distribute, sublicense,
*    and/or sell copies of the Software, and to permit persons to whom the
*    Software is furnished to do so, subject to the following conditions:
*
*    The above copyright notice and this permission notice shall be included in
*    all copies or substantial portions of the Software.
*
*    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
*    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
*    DEALINGS IN THE SOFTWARE.
*
*****************************************************************************/

#include <iostream>
#include <vector>

#include "tim/vx/context.h"
#include "tim/vx/graph.h"
#include "tim/vx/operation.h"
#include "tim/vx/ops/conv2d.h"
#include "tim/vx/tensor.h"

std::vector<uint8_t> input_data = {
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,  0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0,
                    0, 0, 1, 0, 0,
                    0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0,  0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0,
                                    0, 0, 1, 0, 0,
                                    0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,  0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0,
                    0, 0, 1, 0, 0,
                    0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0,  0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0,
                                    0, 0, 1, 0, 0,
                                    0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,  0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0,
                    0, 0, 1, 0, 0,
                    0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0,  0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0,
                                    0, 0, 1, 0, 0,
                                    0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0,
};

uint8_t weights[3*3*9] = {
    1, 2, 3,
    4, 5, 6,
    7, 8, 0,  1, 2, 3,
              4, 5, 6,
              7, 0, 0,  1, 2, 3,
                        4, 5, 6,
                        0, 0, 0,
    1, 2, 3,
    4, 5, 0,
    0, 0, 0,  1, 2, 3,
              4, 0, 0,
              0, 0, 0,  1, 2, 3,
                        0, 0, 0,
                        0, 0, 0,
    1, 2, 0,
    0, 0, 0,
    0, 0, 0,  1, 0, 0,
              0, 0, 0,
              0, 0, 0,  1, 2, 3,
                        4, 5, 6,
                        7, 8, 9,
};

int32_t biases[1] = {
    0,
};

int main(int argc, char** argv) {
  auto context = tim::vx::Context::Create();
  auto graph = context->CreateGraph();

  tim::vx::ShapeType input_shape({5, 5, 9, 1});
  tim::vx::Quantization input_quant(tim::vx::QuantType::ASYMMETRIC, 1.0f, 0);
  tim::vx::TensorSpec input_spec(tim::vx::DataType::UINT8, input_shape,
                                 tim::vx::TensorAttribute::INPUT, input_quant);
  auto input = graph->CreateTensor(input_spec);

  tim::vx::ShapeType weight_shape({3, 3, 9, 1});
  tim::vx::Quantization weighteight_quant(tim::vx::QuantType::ASYMMETRIC, 1.0f, 0);
  tim::vx::TensorSpec weighteight_spec(
      tim::vx::DataType::UINT8, weight_shape,
      tim::vx::TensorAttribute::CONSTANT, weighteight_quant);
  auto weight = graph->CreateTensor(weighteight_spec, &weights[0]);

  tim::vx::ShapeType bias_shape({1});
  tim::vx::Quantization bias_quant(tim::vx::QuantType::ASYMMETRIC, 1.0f, 0);
  tim::vx::TensorSpec bias_spec(
      tim::vx::DataType::INT32, bias_shape,
      tim::vx::TensorAttribute::CONSTANT, bias_quant);
  auto bias = graph->CreateTensor(bias_spec, &biases[0]);

  tim::vx::ShapeType output_shape({3, 3, 1, 1});
  tim::vx::Quantization output_quant(tim::vx::QuantType::ASYMMETRIC, 1.0f, 0);
  tim::vx::TensorSpec output_spec(tim::vx::DataType::UINT8, output_shape,
                                        tim::vx::TensorAttribute::OUTPUT,
                                        output_quant);
  auto output = graph->CreateTensor(output_spec);

  auto conv = graph->CreateOperation<tim::vx::ops::Conv2d>(
      weight_shape[3], tim::vx::PadType::VALID,
      std::array<uint32_t, 2>({2, 2}), std::array<uint32_t, 2>({1, 1}),
      std::array<uint32_t, 2>({1, 1}));
  (*conv)
      .BindInputs({input, weight, bias})
      .BindOutputs({output});

  if (!graph->Compile()) {
    std::cout << "Failed to compile graph." << std::endl;
    return -1;
  }

  if (!input->CopyDataToTensor(input_data.data(), input_data.size())) {
    std::cout << "Failed to copy input data." << std::endl;
    return -1;
  }

  if (!graph->Run()) {
    std::cout << "Failed to run graph." << std::endl;
    return -1;
  }

  std::vector<uint8_t> out;
  out.resize(3 * 3);
  if (!output->CopyDataFromTensor(out.data())) {
    std::cout << "Failed to copy output data." << std::endl;
    return -1;
  }

  printf("\n");
  for (int y = 0; y < 3; y++) {
    printf("|");
    for (int x = 0; x < 3; x++) {
      printf("%02x", out.data()[3 * 3 - 1 - (y * 3 + x)]);
      if (x < 2)
        printf(" ");
    }
    printf("|\n");
  }

  return 0;
}
