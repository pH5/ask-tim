# Ask TIM-VX

... to create compressed weights and bias buffers.

## Download TIM-VX dependency

  git submodule update --init

## Build

  meson setup build
  export LD_LIBRARY_PATH="$PWD/subprojects/TIM-VX/prebuilt-sdk/x86_64_linux/lib"
  meson compile -C build

## Run

e.g.:

  scripts/conv2x2.sh 1 2  3 4
  scripts/conv3x3.sh 1 2 3  4 5 6  7 8 9
  scripts/conv5x5.sh 1 2 3 4 5  6 7 8 9 10  11 12 13 14 15  16 17 18 19 20  21 22 23 24 25

## Analyze

  scripts/nb_dump.py conv2x2.nb
